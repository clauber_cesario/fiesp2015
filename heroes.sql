-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24-Jul-2015 às 03:07
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `heroes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `expedientes`
--

CREATE TABLE IF NOT EXISTS `expedientes` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `hemocentro_id` int(50) DEFAULT NULL,
  `dom` varchar(10) DEFAULT NULL,
  `seg` varchar(10) DEFAULT NULL,
  `ter` varchar(10) DEFAULT NULL,
  `qua` varchar(10) DEFAULT NULL,
  `qui` varchar(10) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `sab` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `expedientes`
--

INSERT INTO `expedientes` (`id`, `hemocentro_id`, `dom`, `seg`, `ter`, `qua`, `qui`, `sex`, `sab`) VALUES
(1, 1, NULL, '8-17', '8-17', '8-17', '8-17', '8-17', '8-13'),
(2, 2, NULL, '8-20', '8-20', '8-20', '8-20', '8-20', '10-16'),
(3, 3, NULL, '7-19', '7-19', '7-19', '7-19', '7-19', '8-15'),
(4, 4, NULL, '8-18', '8-18', '8-18', '8-18', '8-18', '8-12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `hemocentros`
--

CREATE TABLE IF NOT EXISTS `hemocentros` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `nome` varchar(300) DEFAULT NULL,
  `logradouro` varchar(300) DEFAULT NULL,
  `numero` char(6) DEFAULT NULL,
  `complemento` varchar(300) DEFAULT NULL,
  `bairro` varchar(200) DEFAULT NULL,
  `cidade` varchar(200) DEFAULT NULL,
  `estado` varchar(150) DEFAULT NULL,
  `pais` varchar(200) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `orgao` varchar(30) DEFAULT NULL,
  `estacionamento` int(1) DEFAULT NULL,
  `cadastro_medula` int(1) DEFAULT NULL,
  `doacao_aferese` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `hemocentros`
--

INSERT INTO `hemocentros` (`id`, `nome`, `logradouro`, `numero`, `complemento`, `bairro`, `cidade`, `estado`, `pais`, `latitude`, `longitude`, `orgao`, `estacionamento`, `cadastro_medula`, `doacao_aferese`) VALUES
(1, 'Fundação Pró-Sangue Hemocentro de São Paulo - Posto Barueri', 'R. Angela Mirella', '354', 'Térreo', 'Jardim Barueri', 'Barueri', 'São Paulo', 'Brasil', -23.4966324, -46.8728435, 'privado', 1, 1, 1),
(2, 'Fundação Pró-Sangue Hemocentro de São Paulo - Posto Clínicas', 'Av. Enéas Carvalho Aguiar', '155', '1º andar', 'Cerqueira César', 'São Paulo', 'São Paulo', 'Brasil', -23.5570936, -46.6688519, 'privado', 1, 1, 1),
(3, 'Fundação Pró-Sangue Hemocentro de São Paulo - Posto Dante Pazzanese', 'Av. Doutor Dante Pazzanese', '500', NULL, 'Ibirapuera', 'São Paulo', 'São Paulo', 'Brasil', -23.5851063, -46.6522405, 'privado', 1, 1, 1),
(4, 'Fundação Pró-Sangue Hemocentro de São Paulo - Posto Mandaqui', 'R. Voluntários da Pátria', '4227', NULL, 'Mandaqui', 'São Paulo', 'São Paulo', 'Brasil', -23.4845123, -46.6302508, 'privado', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefones`
--

CREATE TABLE IF NOT EXISTS `telefones` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `hemocentro_id` int(50) DEFAULT NULL,
  `ddd` int(2) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `telefones`
--

INSERT INTO `telefones` (`id`, `hemocentro_id`, `ddd`, `telefone`, `tipo`) VALUES
(1, 1, NULL, '0800-55-0300', 0),
(2, 2, NULL, '0800-55-0300', 0),
(3, 3, NULL, '0800-55-0300', 0),
(4, 4, NULL, '0800-55-0300', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
