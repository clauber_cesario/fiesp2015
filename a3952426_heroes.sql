-- MySQL dump 10.13  Distrib 5.1.57, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: a3952426_heroes
-- ------------------------------------------------------
-- Server version	5.1.57
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `BLOOD`
--

DROP TABLE IF EXISTS `BLOOD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BLOOD` (
  `blood_id` int(11) NOT NULL AUTO_INCREMENT,
  `blood_type` char(5) COLLATE latin1_general_ci NOT NULL,
  `rh` tinyint(1) NOT NULL,
  PRIMARY KEY (`blood_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BLOOD`
--

LOCK TABLES `BLOOD` WRITE;
/*!40000 ALTER TABLE `BLOOD` DISABLE KEYS */;
INSERT INTO `BLOOD` VALUES (1,'A',1),(2,'A',0),(3,'B',1),(4,'B',0),(5,'AB',1),(6,'AB',0),(7,'O',1),(8,'O',0);
/*!40000 ALTER TABLE `BLOOD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER`
--

DROP TABLE IF EXISTS `USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USER` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `last_name` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `birth_date` date NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UK_EMAIL_USER` (`email`),
  UNIQUE KEY `UK_USERNAME_USER` (`username`),
  UNIQUE KEY `UK_PASSWORD_USER` (`password`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER`
--

LOCK TABLES `USER` WRITE;
/*!40000 ALTER TABLE `USER` DISABLE KEYS */;
INSERT INTO `USER` VALUES (1,'Clauber','Souza','clauber@hotmail.com.br','clauber','123','0000-00-00');
/*!40000 ALTER TABLE `USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expedientes`
--

DROP TABLE IF EXISTS `expedientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expedientes` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `hemocentro_id` int(50) DEFAULT NULL,
  `dom` varchar(10) DEFAULT NULL,
  `seg` varchar(10) DEFAULT NULL,
  `ter` varchar(10) DEFAULT NULL,
  `qua` varchar(10) DEFAULT NULL,
  `qui` varchar(10) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `sab` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expedientes`
--

LOCK TABLES `expedientes` WRITE;
/*!40000 ALTER TABLE `expedientes` DISABLE KEYS */;
INSERT INTO `expedientes` VALUES (1,1,NULL,'8-17','8-17','8-17','8-17','8-17','8-13'),(2,2,NULL,'8-20','8-20','8-20','8-20','8-20','10-16'),(3,3,NULL,'7-19','7-19','7-19','7-19','7-19','8-15'),(4,4,NULL,'8-18','8-18','8-18','8-18','8-18','8-12');
/*!40000 ALTER TABLE `expedientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hemocentros`
--

DROP TABLE IF EXISTS `hemocentros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hemocentros` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `nome` varchar(300) DEFAULT NULL,
  `logradouro` varchar(300) DEFAULT NULL,
  `numero` char(6) DEFAULT NULL,
  `complemento` varchar(300) DEFAULT NULL,
  `bairro` varchar(200) DEFAULT NULL,
  `cidade` varchar(200) DEFAULT NULL,
  `estado` varchar(150) DEFAULT NULL,
  `pais` varchar(200) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `orgao` varchar(30) DEFAULT NULL,
  `estacionamento` int(1) DEFAULT NULL,
  `cadastro_medula` int(1) DEFAULT NULL,
  `doacao_aferese` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hemocentros`
--

LOCK TABLES `hemocentros` WRITE;
/*!40000 ALTER TABLE `hemocentros` DISABLE KEYS */;
INSERT INTO `hemocentros` VALUES (1,'Fundação Pró-Sangue Hemocentro de São Paulo - Posto Barueri','R. Angela Mirella','354','Térreo','Jardim Barueri','Barueri','São Paulo','Brasil',-23.4966324,-46.8728435,'privado',1,1,1),(2,'Fundação Pró-Sangue Hemocentro de São Paulo - Posto Clínicas','Av. Enéas Carvalho Aguiar','155','1º andar','Cerqueira César','São Paulo','São Paulo','Brasil',-23.5570936,-46.6688519,'privado',1,1,1),(3,'Fundação Pró-Sangue Hemocentro de São Paulo - Posto Dante Pazzanese','Av. Doutor Dante Pazzanese','500',NULL,'Ibirapuera','São Paulo','São Paulo','Brasil',-23.5851063,-46.6522405,'privado',1,1,1),(4,'Fundação Pró-Sangue Hemocentro de São Paulo - Posto Mandaqui','R. Voluntários da Pátria','4227',NULL,'Mandaqui','São Paulo','São Paulo','Brasil',-23.4845123,-46.6302508,'privado',1,1,1);
/*!40000 ALTER TABLE `hemocentros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefones`
--

DROP TABLE IF EXISTS `telefones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefones` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `hemocentro_id` int(50) DEFAULT NULL,
  `ddd` int(2) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefones`
--

LOCK TABLES `telefones` WRITE;
/*!40000 ALTER TABLE `telefones` DISABLE KEYS */;
INSERT INTO `telefones` VALUES (1,1,NULL,'0800-55-0300',0),(2,2,NULL,'0800-55-0300',0),(3,3,NULL,'0800-55-0300',0),(4,4,NULL,'0800-55-0300',0);
/*!40000 ALTER TABLE `telefones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-24 10:40:33
