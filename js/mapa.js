var directionsDisplay;
var directionsService;
var current_pos;
var new_position;
var map;
var geocoder;
var latitude;
var longitude;
var marker;

function replaceAll(str, de, para){
	var pos = str.indexOf(de);
	while (pos > -1){
		str = str.replace(de, para);
		pos = str.indexOf(de);
	}
	return (str);
}

function setCenter(latlng) {

	new_position = latlng;
	new_position = replaceAll(new_position+'', '(', '');
	new_position = replaceAll(new_position, ')', '');
	new_position = new_position.split(', ');

	lat = new_position[0];
	lng = new_position[1];

	new_position = new google.maps.LatLng(lat,lng);

	map.setCenter(new_position);

}

function carregar_mapa(latlng) {

	geocoder = new google.maps.Geocoder();

	directionsDisplay = new google.maps.DirectionsRenderer();

	new_position = latlng;
	new_position = replaceAll(new_position+'', '(', '');
	new_position = replaceAll(new_position, ')', '');
	new_position = new_position.split(', ');

	lat = new_position[0];
	lng = new_position[1];

	new_position = new google.maps.LatLng(-23.469050,-46.611574999999995);

	var mapOptions = {
		center: new_position,
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	map = new google.maps.Map(document.getElementById("mapa"), mapOptions);

	marker = new google.maps.Marker({
		position: new_position,
		map: map
	});

	directionsDisplay.setMap(map);
	directionsDisplay.setPanel(document.getElementById("painel"));

	if(navigator.geolocation){

		navigator.geolocation.getCurrentPosition(function(position) {

			current_pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);

			// var infowindow = new google.maps.InfoWindow({
			// 	map: map,
			// 	position: current_pos,
			// 	content: 'VocÃª estÃ¡ aqui.'
			// });

			map.setCenter(current_pos);
			var marker = new google.maps.Marker({
				position: current_pos,
				map: map,
				title:"VocÃª estÃ¡ aqui"
			});

			rota(new_position,current_pos);

		}, function() {

			/*handleNoGeolocation(true);*/

		});

	}else{

		/*handleNoGeolocation(false);*/

	}

}

function rota(new_position,current_pos) {

	directionsService = new google.maps.DirectionsService();

	var request = {
		origin: current_pos,
		destination: new_position,
		travelMode: google.maps.TravelMode.DRIVING
	};

	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		}
	});

}

/*function coordenadas(endereco) {

	geocoder = new google.maps.Geocoder();

	geocoder.geocode(
		{ 'address': endereco + ', Rio Claro - SP', 'region': 'BRAZIL' },

		function (results, status) {
			
			if(status == google.maps.GeocoderStatus.OK){
				carregar_mapa(results[0].geometry.location);
			}else{}
		
		}

	);

}*/

/*$(document).ready(function() {

	$('#endereco').autocomplete({
		source: function (request, response) {
			geocoder.geocode({ 'address': request.term + ', Rio Claro - SP', 'region': 'BRAZIL' }, function (results, status) {
				response($.map(results, function (item) {
					return {
						label: item.formatted_address,
						value: item.formatted_address,
						latitude: item.geometry.location.lat(),
						longitude: item.geometry.location.lng()
					}
				}));
			})
		},
		select: function (event, ui) {
			current_pos = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
		}
	});

	$("#alterar_origem").click(function() {
		if($("#endereco").val() != ""){
			rota(new_position,current_pos);
		}
	});

});*/

function loadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'http://maps.googleapis.com/maps/api/js?key=AIzaSyDtt-F5_2CX3AI9TyXcALtyrcfz5BgTW6E&sensor=true&' + 'callback=carregar_mapa';
	document.body.appendChild(script);
}

$(document).ready(function() {

	if($('#mapa').length != 0)
		loadScript('-23.5851063, -46.6522405');

});