<?php

include 'conexao.php';

$db = get_db();

header('Content-Type: text/html; charset=utf-8');
$db->query("SET NAMES 'utf8'");
$db->query('SET character_set_connection=utf8');
$db->query('SET character_set_client=utf8');
$db->query('SET character_set_results=utf8');

$id = $_GET['hemocentro_id'];

$query = $db->query(
	"SELECT * FROM expedientes WHERE hemocentro_id = " . $id
);

$expediente = array();

while($expediente = $query->fetch(PDO::FETCH_OBJ)){

	$exp = array(
		'id'				=> $expediente->id,
		'hemocentro_id'		=> $expediente->hemocentro_id,
		'dom'				=> $expediente->dom,
		'seg'				=> $expediente->seg,
		'ter'				=> $expediente->ter,
		'qua'				=> $expediente->qua,
		'qui'				=> $expediente->qui,
		'sex'				=> $expediente->sex,
		'sab'				=> $expediente->sab
	);

}

echo json_encode($exp);

?>