$(document).ready(function() {

	$('[data-toggle="tooltip"]').tooltip();

	var w = $('#nav-lateral').width();

	if($(window).width() <= 768){
		$('#nav-lateral').addClass('nav-hide');
		$('#content').attr('style','padding-left:'+w+'px');
	}

	$(window).load(function() {
		$('#nav-lateral').height($(document).height());
	});

	function checkDocumentHeight(callback){
		var lastHeight = document.body.clientHeight, newHeight, timer;
		(function run(){
			newHeight = document.body.clientHeight;
				if( lastHeight != newHeight )
					callback();
			lastHeight = newHeight;
			timer = setTimeout(run, 200);
		})();
	}

	function doSomething(){
		$('#nav-lateral').height($(document).height());
	}

	checkDocumentHeight(doSomething);

	$('.toggle-nav').click(function() {
		if($('#nav-lateral').hasClass('nav-hide')){
			$('#nav-lateral').removeClass('nav-hide');
			$('#content').attr('style','padding-left:'+w+'px');
		}else{
			$('#nav-lateral').addClass('nav-hide');
			$('#content').attr('style','padding-left:0px');
		}
	});

	$(".responsive-calendar").responsiveCalendar({
		time: ano+'-'+mes,
		translateMonths: ["Janeiro","Fevereiro","Março","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
		startFromSunday: true,
		monthChangeAnimation: false
	});

	var directionsDisplay;
	var directionsService;
	var current_pos;
	var new_position;
	var initialLocation;
	var map;
	var geocoder;
	var latitude;
	var longitude;

	var coordenadas;
	var coord_minha;
	$('[data-toggle="magnificPopup"]').click(function(e) {

		var nome = $('h3', this).text();
		var rel = $(this).attr('rel');

		var src  = '<div class="white-popup" id="hemocentro" rel="'+rel+'">';
			src += '<h4 class="modal-title">'+nome+'</h4>';
			src += '<div id="mapa" style="height:450px;width:100%;"></div>';
			src += '<div class="text-center"><button type="button" class="btn btn-primary agendamento">Agendamento</button></div>';
			src += '</div>';

		e.preventDefault();
		coordenadas = $(this).attr('latitude')+','+$(this).attr('longitude');
		coord_minha = $(this).attr('minha_lat')+','+$(this).attr('minha_long');
		$.magnificPopup.open({
			items: {
				src: src,
				type: 'inline'
			},
			closeBtnInside: true
		});
	});

	$(document).on('mfpOpen', function() {
		gerar_mapa();
	});

	var calendario  = '<div class="responsive-calendar">';
		calendario += '<div class="controls">';
		calendario += '<a class="pull-left" data-go="prev">';
		calendario += '<div class="btn"><i class="glyphicon glyphicon-chevron-left"></i></div>';
		calendario += '</a>';
		calendario += '<h4>';
		calendario += '<span data-head-year></span>';
		calendario += '<span data-head-month></span>';
		calendario += '</h4>';
		calendario += '<a class="pull-right" data-go="next">';
		calendario += '<div class="btn"><i class="glyphicon glyphicon-chevron-right"></i></div>';
		calendario += '</a>';
		calendario += '</div>';
		calendario += '<hr/>';
		calendario += '<div class="day-headers">';
		calendario += '<div class="day header">D</div>';
		calendario += '<div class="day header">S</div>';
		calendario += '<div class="day header">T</div>';
		calendario += '<div class="day header">Q</div>';
		calendario += '<div class="day header">Q</div>';
		calendario += '<div class="day header">S</div>';
		calendario += '<div class="day header">S</div>';
		calendario += '</div>';
		calendario += '<div class="days" data-group="days"></div>';
		calendario += '</div>';

	var horarios = '';

	var ano = new Date().getFullYear();
	var mes = new Date().getMonth();

	$(document).on('click', '.agendamento', function() {

		var nome = $(this).closest('#hemocentro').find('.modal-title').text();
		var rel = $(this).closest('#hemocentro').attr('rel');

		var agendamento_html  = '<h4 class="modal-title">'+nome+'</h4>';
			agendamento_html += '<div class="row"><div class="col-md-8 col-sm-6">'+calendario+'</div><div class="col-md-4 col-sm-6 horarios-funcionamento">'+horarios+'</div></div>';
			agendamento_html += '<button title="Close (Esc)" type="button" class="mfp-close">×</button>';
			agendamento_html += '<form action="#" id="form-agendamento"><input type="hidden" name="user_id"><input type="hidden" name="hemocentro_id"><input type="hidden" name="data"><input type="hidden" name="hora"><input type="hidden" name="tipo"></form>';
		$('#hemocentro').html(agendamento_html);

		var dom,seg,ter,qua,qui,sex,sab;
		$(".responsive-calendar").responsiveCalendar({
			time: ano+'-'+mes,
			allRows: false,
			translateMonths: ["Janeiro","Fevereiro","Março","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
			startFromSunday: true,
			monthChangeAnimation: false,
			onInit: function() {
				$.ajax({
					url: "getExpedientes.php?hemocentro_id=" + rel,
					dataType: "json",
					success: function(result) {
						dom=result['dom'];
						seg=result['seg'];
						ter=result['ter'];
						qua=result['qua'];
						qui=result['qui'];
						sex=result['sex'];
						sab=result['sab'];
						if(!result['dom']){
							$('.days .day.sun').addClass('disabled');
						}
					}
				});
			},
			onDayClick: function() {
				$('.day-active').removeClass('day-active');
				$(this).addClass('day-active');
				if($(this).parent().hasClass('sun'))
					exp = dom;
				if($(this).parent().hasClass('mon'))
					exp = seg;
				if($(this).parent().hasClass('tue'))
					exp = ter;
				if($(this).parent().hasClass('wed'))
					exp = qua;
				if($(this).parent().hasClass('thu'))
					exp = qui;
				if($(this).parent().hasClass('fri'))
					exp = sex;
				if($(this).parent().hasClass('sat'))
					exp = sab;

				de_ate = exp.split('-');
				de = (de_ate[0].length == 1 ? '0'+de_ate[0] : de_ate[0]);
				ate = (de_ate[1].length == 1 ? '0'+de_ate[1] : de_ate[1]);

				funciona = '<h3 class="titulo-horarios">Das '+de+' até '+ate+'</h3>';

				var horas = '';
				for(i=de;i<ate;i++){
					hora = (i.length==1 ? '0'+i : i);
					horas += '<div class="col-md-4 col-sm-4 col-xs-6"><p>'+hora+':00</p></div>';
				}

				horarios = '<div class="row">'+funciona+horas+'</div>';
				horarios += '<div class="row"><div class="tipos-doacao"></div></div>';
				horarios += '<div class="row"><div class="col-md-12"><div class="agendar-doacao"></div></div></div>';

				$('.horarios-funcionamento').html(horarios);

			}
		});
	});

	$(document).on('click', '.horarios-funcionamento p', function() {
		var tipos	 = '<div class="col-md-6 col-sm-6 col-xs-6"><p>Doação Sangue Total</p></div>';
			tipos	+= '<div class="col-md-6 col-sm-6 col-xs-6"><p>Doação por Aférese</p></div>';
		$('.tipos-doacao').html(tipos);
	});

	$(document).on('click', '.tipos-doacao p', function() {
		var btn = '<div class="row"><div class="col-md-12"><button type="button" class="btn btn-primary btn-agendar-doacao">AGENDAR</button></div></div>';
		$('.agendar-doacao').html(btn);
	});

	function gerar_mapa() {
		carregar_mapa(coordenadas,coord_minha);
	}
	
	var directionDisplay;
	var directionsService = new google.maps.DirectionsService();
	var map;
 
	function carregar_mapa(local_hemo,coord_minha) {
		directionsDisplay = new google.maps.DirectionsRenderer();
		var myLatlng = new google.maps.LatLng();
		 
		var myOptions = {
			zoom:7,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: myLatlng
		}

		map = new google.maps.Map(document.getElementById("mapa"), myOptions);
		calcRoute(local_hemo,coord_minha);
		directionsDisplay.setMap(map);
	}
 
	function calcRoute(local_hemo,coord_minha) {
		var start = coord_minha;//document.getElementById("endereco").value;
		var end = local_hemo;//document.getElementById("destino").value;
		var request = {
			origin:start, 
			destination:end,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};
		 
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			} else {
				alert(status);
			}

		    //document.getElementById('mapview').style.visibility = 'visible';
		});
	}

	$(window).load(function() {
		$('.bg-coracao').addClass('active');
	});
/*
	function carregar_mapa(latlng) {

		geocoder = new google.maps.Geocoder();

		directionsDisplay = new google.maps.DirectionsRenderer();

		new_position = latlng;
		new_position = new_position.split(',');

		lat = new_position[0];
		lng = new_position[1];
		new_position = new google.maps.LatLng(lat,lng);
		
		minha_pos = "-23.469050,-46.61157499";
		minha_pos =  minha_pos.split(',');
		minha_pos = new google.maps.LatLng(minha_pos[0],minha_pos[1]);
		var end1 = minha_pos[0]; ;
		var mapOptions = {
			origin:new_position, 
			destination:minha_pos,
			zoom: 15,
			scrollwheel: false,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};

		map = new google.maps.Map(document.getElementById("mapa"), mapOptions);

		var marker = new google.maps.Marker({
			position: new_position,
			map: map
		});

		directionsDisplay.setMap(map);

	}
*/
});